# WTTJ // Mark Trello demo

Application simulant le comportement de Trello en React avec du temps réel.

## Frontend

Le serveur est celui de développement.
```
cd frontend_wttj
npm install
yarn start
```

## Backend

Le backend est composé de plusieurs composants.
- Un server rails pour gérer les données, la persistence et la logique metier
- Un server en Node.js pour l'échange en temps réel
Les serveurs communiquent entre eux à travers Redis.

Il convient d'utiliser docker compose pour lancer tous les éléments.

```
docker-compose build
docker-compose up
```
Pour fermer ctrl+c ou
```
docker-compose stop
```
