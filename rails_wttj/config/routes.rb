Rails.application.routes.draw do
  get 'api/cards'
  put 'api/card', to: 'api#update_card'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
