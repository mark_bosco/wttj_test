FactoryGirl.define do
  factory :card do
    name { Faker::GameOfThrones.character }
    description { Faker::Job.title }
    status :to_meet
    index_position 1
  end
end
