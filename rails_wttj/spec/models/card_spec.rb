require 'rails_helper'

RSpec.describe Card, type: :model do
  context 'when we update our card position' do
    context 'and it got the same state' do
      it 'update its position from 1 to 3' do
        card1 = create(:card, index_position: 1)
        card2 = create(:card, index_position: 2)
        card3 = create(:card, index_position: 3)
        card4 = create(:card, index_position: 4)

        card1.index_position = 3
        card1.save

        card2.reload
        card3.reload
        card4.reload

        expect(card2.index_position).to eq(1)
        expect(card1.index_position).to eq(3)
        expect(card3.index_position).to eq(2)
        expect(card4.index_position).to eq(4)
      end

      it 'update its position from 3 to 2' do
        card1 = create(:card, index_position: 1)
        card2 = create(:card, index_position: 2)
        card3 = create(:card, index_position: 3)
        card4 = create(:card, index_position: 4)

        card3.index_position = 2
        card3.save

        card1.reload
        card2.reload
        card4.reload

        expect(card1.index_position).to eq(1)
        expect(card2.index_position).to eq(3)
        expect(card3.index_position).to eq(2)
        expect(card4.index_position).to eq(4)
      end
    end

    context 'and it got a different state' do
      it 'update its state and its position from 1 to 2' do
        to_meet_card1 = create(:card, index_position: 1)
        to_meet_card2 = create(:card, index_position: 2)
        to_meet_card3 = create(:card, index_position: 3)

        interviewed_card1 = create(:card, index_position: 1, status: :interviewed)
        interviewed_card2 = create(:card, index_position: 2, status: :interviewed)

        to_meet_card1.status = :interviewed
        to_meet_card1.index_position = 2
        to_meet_card1.save

        to_meet_card2.reload
        to_meet_card3.reload
        interviewed_card1.reload
        interviewed_card2.reload

        expect(to_meet_card1.index_position).to eq(2)
        expect(to_meet_card1.status).to eq('interviewed')
        expect(interviewed_card2.index_position).to eq(3)
        expect(interviewed_card1.index_position).to eq(1)
        expect(to_meet_card2.index_position).to eq(1)
        expect(to_meet_card3.index_position).to eq(2)
      end
    end
  end
end
