class ApiController < ApplicationController

  def cards
    render json: Card.all
  end

  def update_card
    # we load our card
    card = Card.find(card_params["card_id"])
    if card
      card.index_position = card_params["index"]
      card.status = card_params["status"]
      card.save
      # once the infos are updated we publish the modification
      $redis.publish('cards', "updated")

      render json: card
      return
    end

    render nothing: true, status: 400
  end

  def card_params
    params.permit(:card_id, :status, :index)
  end
end
