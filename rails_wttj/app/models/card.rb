# Card class
class Card < ApplicationRecord
  enum status: [:to_meet, :interviewed]
  before_update :update_cards_idx, if: :should_update?
  before_update :update_all_cards, if: :status_changed?

  # Update our cards index based on the new card status and index
  def update_all_cards
    update_all_cards_index_for_new_status
    update_all_cards_index_for_previous_status
  end

  def should_update?
    index_position_changed? && !status_changed?
  end

  # Update our card idx for the status
  def update_cards_idx
    if index_position_was < index_position
      update_all_cards_after_index
    else
      update_all_cards_before_index
    end
  end

  # Update cards for the same status
  def update_all_cards_after_index
    Card.where(status: status)
        .where('index_position <= ? AND index_position > ?', index_position, index_position_was)
        .update_all('index_position = index_position - 1')
  end

  # Update cards for the same status
  def update_all_cards_before_index
    Card.where(status: status)
        .where('index_position < ? AND index_position >= ?', index_position_was, index_position)
        .update_all('index_position = index_position + 1')
  end

  # Update cards for the new status
  def update_all_cards_index_for_new_status
    Card.where(status: status)
        .where('index_position >= ?', index_position)
        .update_all('index_position = index_position + 1')
  end

  # Update cards for the new status
  def update_all_cards_index_for_previous_status
    Card.where(status: status_was)
        .where('index_position > ?', index_position_was)
        .update_all('index_position = index_position - 1')
  end
end
