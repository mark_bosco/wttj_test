(1..3).each do |n|
  Card.create(
    name: Faker::GameOfThrones.character,
    description: Faker::Job.title,
    status: :to_meet,
    index_position: n
  )
end

(1..2).each do |n|
  Card.create(
    name: Faker::GameOfThrones.character,
    description: Faker::Job.title,
    status: :interviewed,
    index_position: n
  )
end
