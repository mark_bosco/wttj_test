import {createStore, applyMiddleware} from 'redux';
// Import thunk middleware
import thunk from 'redux-thunk';
import mainReducer from '../reducers/MainReducer'

export default function configureStore(initialState) {
  return createStore(mainReducer, initialState,
    // Apply to store
    applyMiddleware(thunk)
  );
}
