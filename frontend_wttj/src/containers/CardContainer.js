import React, {Component} from 'react';
import PropTypes from 'prop-types'
import Badge from 'material-ui/Badge';
import {Section, Header, Title } from '../styles/Base'
import { connect } from 'react-redux'
import DraggableCard from './DraggableCard'
import {ItemTypes} from '../constants/ItemTypes';
import { DropTarget } from 'react-dnd';
import { putMoveCard } from '../actions';


class CardContainer extends Component {

  state = {
    cards: this.props.cards,
    placeholderIndex: -1
  }

  renderCards = () => {
    const cardList = this.props.cards.map((card, idx) =>
      <DraggableCard
        key={card.id}
        index={idx}
        containerId={this.props.id}
        {...card}
      />
    )
    return cardList
  }

  countCards = () => {
    return this.props.cards.length
  }

  render () {
    const { connectDropTarget } = this.props;
    return connectDropTarget(
      <div style={{
        position: 'relative',
        width: '50%',
        height: '100%'
      }}>
        <Section>
          <Header>
            <Title>
              {this.props.title}
              <Badge
                badgeContent={this.countCards()}
                badgeStyle={{top: 15, right: 2, backgroundColor: '#abb5c9'}}
              />
            </Title>

          </Header>
          {this.renderCards()}
        </Section>
      </div>
    )
  }
}

CardContainer.propTypes = {
  title: PropTypes.string,
  id: PropTypes.string.isRequired,
  cards: PropTypes.array,
  connectDropTarget: PropTypes.func.isRequired,
}


const containerTarget = {
  drop(props, monitor) {
    const {id} = props
    const draggedData = monitor.getItem()
    props.onCardMove(draggedData)
    return {
      containerId: id
    }
  }
};

function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget()
  };
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onCardMove:(draggedData) => {
      dispatch(
        putMoveCard(
        {
          draggedCard: draggedData.card,
          newStatus: ownProps.id,
          index: 1
        }))
    }
  }
}

export default connect(null, mapDispatchToProps)(DropTarget(ItemTypes.CARD, containerTarget, collect)(CardContainer))
