import React, {Component} from 'react'
import {BoardDiv} from '../styles/Base'
import CardContainer from './CardContainer'
import {toInterview, toMeet} from '../selectors/Card'
import Header from '../components/Header/Header'
import { connect } from 'react-redux'
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import io from "socket.io-client"
import { fetchCards } from '../actions';

let socket;

class MainContainer extends Component {

  constructor(props)
  {
    super(props)
    socket = io.connect("http://localhost:5000")
    if (socket) {
     socket.on('cardUpdate',(data)=>{
       props.refetchCards()
     })
    }
  }

  componentWillUnmount() {
    socket.disconnect()
  }

  render () {
    return (
      <div>
        <Header />
        <BoardDiv>
          <CardContainer title={"À rencontrer"} id="to_meet" cards={this.props.toMeetCards}/>
          <CardContainer title={"Entretien"} id="interviewed" cards={this.props.interviewCards}/>
        </BoardDiv>
      </div>
    )
  }
}


const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    refetchCards:(data) => {
      dispatch(fetchCards())
    }
  }
}

const mapStateToProps = state => {
  return {
    interviewCards: toInterview(state),
    toMeetCards: toMeet(state)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DragDropContext(HTML5Backend)(MainContainer))
