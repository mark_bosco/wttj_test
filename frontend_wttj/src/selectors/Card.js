import { createSelector } from 'reselect'

const cardSelector = state => state.cards

const toInterview = createSelector(
  cardSelector,
  cards => cards.filter(function(card) {
    return card.status === 'interviewed';
  }).sort(function(a, b) {
      return a.index_position - b.index_position;
  })
)

const toMeet = createSelector(
  cardSelector,
  cards => cards.filter(function(card) {
      return card.status === 'to_meet';
    }).sort(function(a, b) {
        return a.index_position - b.index_position;
    })
)

export {
  toInterview,
  toMeet,
};
