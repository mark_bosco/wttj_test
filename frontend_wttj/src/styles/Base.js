import styled from 'styled-components'

export const Section = styled.section`
  background-color: #fffcff;
  border-radius: 3px;
  margin: 5px 5px;
  padding: 10px;
  min-height: 950px;
  height: auto;
  max-height: 95%;
  overflow-y: auto;
`

export const Header = styled.header`
  margin-bottom: 10px;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
`

export const Title = styled.span`
  font-weight: bold;
  font-size: 15px;
  line-height: 18px;
  cursor: grab;
  width: 80%;
`

export const ImageHeader = styled.img`
  width: 50px;
  height: 50px;
  margin: 5px 15px;
`

export const BoardDiv = styled.div`
  background-color: #efedef;
  overflow-y: hidden;
  padding: 5px;
  font: 14px/18px "Helvetica Neue", Arial, Helvetica, sans-serif;
  color: #393939;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  height: 100vh;
`
