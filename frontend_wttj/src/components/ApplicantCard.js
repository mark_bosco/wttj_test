import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {Card, CardActions, CardTitle, CardText} from 'material-ui/Card';
import Avatar from 'material-ui/Avatar';
import IconButton from 'material-ui/IconButton';
import Thumb from 'material-ui/svg-icons/action/thumb-up';
import Message from 'material-ui/svg-icons/communication/message';
import Email from 'material-ui/svg-icons/communication/email';
import {grey400} from 'material-ui/styles/colors';

const style = {margin: 20, float: 'left'};
const cardStyle = {margin: 10};

class ApplicantCard extends Component {

  render () {
    const {name, description} = this.props
    return (
      <Card style={cardStyle}>
        <Avatar
            src="https://robohash.org/wttj"
            size={60}
            style={style}
          />
        <CardTitle title={name}/>
        <CardText>
          {description}
        </CardText>
        <CardActions>
          <IconButton>
            <Thumb
              color={grey400}/>
          </IconButton>
          <IconButton>
            <Message
              color={grey400} />
          </IconButton>
          <IconButton>
            <Email
              color={grey400} />
          </IconButton>
        </CardActions>
      </Card>
    )
  }
}

ApplicantCard.propTypes = {
  id: PropTypes.number.isRequired,
  index: PropTypes.number.isRequired,
  name: PropTypes.string,
  state: PropTypes.string,
  description: PropTypes.string,
}

export default ApplicantCard;
