import React from 'react';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import MenuHome from 'material-ui/svg-icons/navigation/menu';
import Notifications from 'material-ui/svg-icons/social/notifications';
/**
 * A simple example of `AppBar` with an icon on the right.
 * By default, the left icon is a navigation-menu.
 */
const SubHeader = () => (
  <AppBar
    title="Stage Account Manager"
    style={{backgroundColor: '#334773'}}
    iconElementLeft={<IconButton
      >
      <MenuHome />
    </IconButton>}
    iconElementRight={<IconButton
      >
      <Notifications />
    </IconButton>}
  />
);

export default SubHeader;
