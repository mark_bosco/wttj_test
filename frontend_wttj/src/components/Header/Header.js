import React from 'react';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import MenuHome from 'material-ui/svg-icons/navigation/menu';
import SubHeader from './SubHeader';
import {ImageHeader} from '../../styles/Base'
/**
 * A simple example of `AppBar` with an icon on the right.
 * By default, the left icon is a navigation-menu.
 */
const Header = () => (
  <div>
    <AppBar
      title={<ImageHeader src="https://www.welcometothejungle.co/uploads/company/logo/wttj.png"/>}
      showMenuIconButton={false}
      style={{backgroundColor: '#2eaa7e'}}
      iconElementRight={<IconButton>
        <MenuHome />
      </IconButton>}
    />
    <SubHeader />
  </div>
);

export default Header;
