import {createAction} from 'redux-actions'
import axios from 'axios';

export const moveCard = createAction('MOVE_CARD')
export const initCards = createAction('INIT_CARDS')

export const fetchCards = () => {
  // Returns a dispatcher function
  return (dispatch) => {
    // Returns a promise
    return axios.get("http://localhost:4000/api/cards")
      .then(response => {
        dispatch(initCards({cards: response.data}))
      })
      .catch(error => {
        throw(error);
      });
  };
};

export const putMoveCard = ({draggedCard, newStatus, index}) => {
  // Returns a dispatcher function
  return (dispatch) => {
    // Returns a promise
    axios.put("http://localhost:4000/api/card", {
      card_id: draggedCard.id,
      status: newStatus,
      index: index
    })
    .then(response => {
      console.log(response)
      dispatch(moveCard({draggedCard, newStatus, index}))
    })
    .catch(error => {
      throw(error);
    });
  };
};
