
function moveCard(state, {draggedCard, newStatus, index}) {
    return {
        ...state,
        cards: state.cards.map(card => card.id === draggedCard.id ?
            { ...card, status: newStatus } :
            card
        )
    };
}

function initializeCards(state, cards) {
  return cards;
}

const mainReducer = (state = {
  cards: []
}, action) => {
  const {payload, type} = action
  switch (type) {
    case 'MOVE_CARD':
      return moveCard(state, payload)
    case 'INIT_CARDS':
      return initializeCards(state, payload)
    default:
      return state
  }
}

export default mainReducer
