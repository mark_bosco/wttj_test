import React, { Component } from 'react';
import MainContainer from './containers/MainContainer'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {Provider} from 'react-redux'
import configureStore from './store/configureStore'
import { fetchCards } from './actions';

const store = configureStore();
store.dispatch(fetchCards());

class App extends Component {
  render() {
    return (
      <MuiThemeProvider>
        <Provider store={store}>
          <MainContainer/>
        </Provider>
      </MuiThemeProvider>
    );
  }
}

export default App;
