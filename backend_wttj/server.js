const http = require('http')
const socketServer =require('socket.io')
var redis = require("redis"),
    client = redis.createClient({url: "redis://redis:6379"});

var serve = http.createServer();
var io = socketServer(serve);
serve.listen(5000, ()=> {})

client.subscribe('cards');


const connections = [];
io.on('connection', function (socket) {
	connections.push(socket)
  client.on('message', function(channel, message) {
    io.emit('cardUpdate')
  });
	socket.on('disconnect', function(){
	});

});
